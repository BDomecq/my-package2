# CHANGELOG



## v0.2.1 (2024-03-16)

### Fix

* fix(deploy): add deploy pipeline ([`a9cfbc6`](https://gitlab.com/BDomecq/my-package2/-/commit/a9cfbc630b473e82d0044703c4410530420e5303))


## v0.2.0 (2024-03-16)

### Feature

* feat(addition): add addition feature ([`dd0bf88`](https://gitlab.com/BDomecq/my-package2/-/commit/dd0bf88a89900b0567f21c3bcd5779bcad8ffaf8))


## v0.1.0 (2024-03-16)

### Feature

* feat(sr): add semantic release cd pipeline ([`7e9dee6`](https://gitlab.com/BDomecq/my-package2/-/commit/7e9dee6b170a655a4c3f02dff4e7b6e8e2d53fce))

### Unknown

* add semantic_release config ([`51fa884`](https://gitlab.com/BDomecq/my-package2/-/commit/51fa884100291d094c43c66a89f1b43bddc29f70))

* Update pyproject.toml ([`57a57ae`](https://gitlab.com/BDomecq/my-package2/-/commit/57a57ae24a9e1443d19aea6ede8c606110470829))

* Add new file ([`0048572`](https://gitlab.com/BDomecq/my-package2/-/commit/00485722f4e9d0ae2f90208efa6c74396967a3c2))

* Update .gitlab-ci.yml file ([`d1c6d20`](https://gitlab.com/BDomecq/my-package2/-/commit/d1c6d20cb7574777104e44d5fbc89518ea518204))

* Add new file ([`153784c`](https://gitlab.com/BDomecq/my-package2/-/commit/153784cfdfeabf0a5995b73b6fc35042d51601a9))

* Initial commit ([`24a1fca`](https://gitlab.com/BDomecq/my-package2/-/commit/24a1fca91ed31b57da753a591e85a368a95fae3d))
